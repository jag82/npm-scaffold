const test = require('tape-async');
const { execSync } = require('child_process');
const fs = require('fs');

const { 
    resetTestDir,
    getTestConfig,
    exists,
    existsNot,
    read,
    readJson,
    contains,
    containsNot,
    containsNotDirectives
} = require('../utils.js');


resetTestDir();

test('generateReadme', async function(t) {
    const config = getTestConfig();
    const generateReadme = require('../../src/generator/generateReadme.js');

    await generateReadme(config);

    exists(t, 'README.md');
    const readmeMd = read('README.md');
    containsNotDirectives(t, readmeMd);

    resetTestDir();
    t.end();
});


test.only('generateSettings', async function(t) {
    const config = getTestConfig();
    const generateSettings = require('../../src/generator/generateSettings.js');

    await generateSettings(config);

    exists(t, 'settings.json');
    const settingsJson = read('settings.json');
    containsNotDirectives(t, settingsJson);

    resetTestDir();
    t.end();
});

