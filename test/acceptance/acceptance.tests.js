const test = require('tape-async');
const { execSync } = require('child_process');
const fs = require('fs');

// name of our generated test projects
const TEST_PKG = '_testpkg';

// delete our scaffolded packages that we generate as a test
destroyScaffold();

test('setup package.json', function(t) {
    createScaffold(''); //cannot toggle package json off!

    exists(t, 'package.json');

    const pkg = readJson('package.json');
    t.ok(typeof pkg === 'object', 'package.json is object');
    t.ok(pkg.name, 'has name');
    t.ok(pkg.version, 'has version');
    t.ok(pkg.description, 'has description');
    t.ok(pkg.scripts, 'has scripts');
    t.ok(pkg.author, 'has author');
    t.ok(pkg.license, 'has license');
    t.ok(pkg.dependencies, 'has dependencies');
    t.ok(pkg.devDependencies, 'has devDependencies');

    // settings.json
    // t.ok(fs.existsSync('testpkg/settings.json'), 'settings exist');
    // t.deepEqual(require('../testpkg/settings.json'), { googleApiKey: 'LongApiKeyGoesHere' }, 'settings is a json object')

    // constants.js
    exists(t, 'app/constants.js');

    destroyScaffold();
    t.end();
});

test('setup README.md', function(t) {
    createScaffold('--readme');

    exists(t, 'README.md');

    const readme = read('README.md');
    contains(t, readme, `\# ${TEST_PKG}`, 'has title');
    contains(t, readme, '\#\# Installation', 'has installation section');
    contains(t, readme, '\#\# Usage', 'has usage section');

    destroyScaffold();
    t.end();
});

test('setup a linter', function(t) {
    createScaffold('--linter');

    exists(t, '.eslintrc.json');

    const eslintrc = readJson('.eslintrc.json');
    t.ok(typeof eslintrc === 'object', 'is object');
    t.ok(eslintrc.env, 'has env');
    t.ok(eslintrc.parserOptions, 'has parserOptions');
    t.ok(eslintrc.rules, 'has rules');

    destroyScaffold();
    t.end();
});

//TODO: reimplement this once we have remote repos added to the config
test.skip('setup a repo', function(t) {
    createScaffold('--repo');

    exists(t, '.gitignore');

    const gitignore = read('.gitignore');
    contains(t, gitignore, /node_modules\n/, 'repo ignores npm dependencies');

    const pkg = readJson('package.json');
    t.ok(pkg.repository, 'package.json has repository');
    t.ok(pkg.repository.type, 'package.json has repository.type');
    t.ok(pkg.repository.url, 'package.json has repository.url');
    t.ok(pkg.bugs, 'package.json has bugs');
    t.ok(pkg.bugs.url, 'package.json has bugs.url');
    t.ok(pkg.homepage, 'package.json has homepage');

    destroyScaffold();
    t.end();
});

test('setup tests', function(t) {
    createScaffold('--tests');

    const pkg = readJson('package.json');
    t.ok(pkg.scripts.test, 'has a script to run tests');
    t.ok(pkg.scripts['test-watch'], 'has a watch script to trigger tests');
    t.ok(pkg.devDependencies['chokidar-cli'], 'uses file watcher (chokidar-cli)');
    t.ok(pkg.devDependencies['tap-notify'], 'uses notifier for test results (tap-notify)');
    t.ok(pkg.devDependencies['tap-spec'], 'uses reporter for tap output (tap-spec)');
    t.ok(pkg.devDependencies['tape'], 'uses test runner (tape)');
    t.ok(pkg.devDependencies['tape-async'], 'uses test runner exception catcher + async support (tape-async)');

    exists(t, 'test/all.tests.js', 'has sample tests');

    destroyScaffold();
    t.end();
});

test('setup a database (db)', function(t) {
    createScaffold('--db');

    const pkg = readJson('package.json');
    t.ok(pkg.dependencies.mongoose, 'uses db helper (mongoose)');

    exists(t, 'app/core/db.js', 'has db connection entry point');

    exists(t, 'app/core/models/data.js', 'has sample collection (CRUD)');
    exists(t, 'app/core/models/users.js', 'has users collection');
    
    destroyScaffold();
    t.end();
});

test('setup file storage (files)', function(t) {
    createScaffold('--files');

    exists(t, 'app/files/default/penguin.gif', 'has sample files for download');

    // destroyScaffold();
    t.end();
});

test('setup a command line interface (cli)', function(t) {
    createScaffold('--cli');

    const pkg = readJson('package.json');
    t.ok(pkg.bin, 'package.json points to binary file');
    t.ok(pkg.dependencies.yargs, 'uses argument parser (yargs)');
    t.ok(pkg.dependencies.inquirer, 'uses prompt for user input (inquirer)');

    // bin is named same as the package's name
    const bin = read(`app/cli/bin/${TEST_PKG}`);
    contains(t, bin, /^#!\/usr\/bin\/env node/, 'bin declares itself to use node: #!/usr/bin/env node');
    contains(t, bin, /require\('\.\.\/\.\.\/cli\.js'\)/, "bin defines js entry point: require('../cli.js')");

    exists(t, 'app/cli.js', 'has js entry point');

    exists(t, 'app/cli/commands/speak.js', 'has a sample command');
    exists(t, 'app/cli/commands/listen.js', 'has a sample command that accepts user input');

    destroyScaffold();
    t.end();
});

test('setup a web app (web)', function(t) {
    createScaffold('--web');

    exists(t, 'settings.json');

    const gitignore = read('.gitignore');
    contains(t, gitignore, /settings\.json\n/, 'repo ignores sensitive info');

    const pkg = readJson('package.json');
    t.ok(pkg.scripts['web-start'], 'has a script for quickly starting + dev deployment');
    t.ok(pkg.scripts['web-live'], 'has a script for live deployment');
    t.ok(pkg.scripts['web-watch'], 'has a script to watch for dev changes');
    t.ok(pkg.scripts['web-debug'], 'has a script to run debugger');
    t.ok(pkg.dependencies.express, 'uses web server (express)');
    //TODO: only true if config.files!
    t.ok(pkg.dependencies.multer, 'uses file uploader (multer)');
    t.ok(pkg.dependencies['express-session'], 'uses session manager (express-session)');
    t.ok(pkg.dependencies['session-file-store'], 'uses session store (session-file-store)');
    t.ok(pkg.dependencies['node-cron'], 'uses cron jobs (node-cron)');
    t.ok(pkg.devDependencies.nodemon, 'uses server hot-reload (nodemon)');

    exists(t, 'app/web.js', 'has js entry point');

    //TODO: write more test for server + client code
    //server
    exists(t, 'app/web/server/setup.sh', 'has script for env depdencies (todo:)');
    exists(t, 'app/web/server/app.js', 'has app code (separate from entry point)');
    exists(t, 'app/web/server/cron-jobs.js', 'has cronjob scripts');

    //client
    exists(t, 'app/web/client/index.html');
    exists(t, 'app/web/client/index.js');
    exists(t, 'app/web/client/index.css');
    exists(t, 'app/web/client/pages/login/login.html', 'has login page');
    exists(t, 'app/web/client/js/jquery-3.3.1.min.js', 'contains js helpers (jquery)');
    exists(t, 'app/web/client/styles/fontawesome.min.css', 'contains icon library (fontawesome css)');
    exists(t, 'app/web/client/webfonts/', 'contains icon library (fontawesome fonts)');
    exists(t, 'app/web/client/favicon/', 'contains favicons');

    exists(t, 'app/web/client/pages/create/create.html');
    exists(t, 'app/web/client/pages/create/create.js');
    exists(t, 'app/web/client/pages/create/create.css');

    exists(t, 'app/web/client/pages/edit/edit.html');
    exists(t, 'app/web/client/pages/edit/edit.js');
    exists(t, 'app/web/client/pages/edit/edit.css');

    exists(t, 'app/web/client/pages/list/list.html');
    exists(t, 'app/web/client/pages/list/list.js');
    exists(t, 'app/web/client/pages/list/list.css');

    exists(t, 'app/web/client/pages/view/view.html');
    exists(t, 'app/web/client/pages/view/view.js');
    exists(t, 'app/web/client/pages/view/view.css');

    destroyScaffold();
    t.end();
});



function createScaffold(args) {
    // since we expect users to either:
    //   npm link (development)
    //   npm i -g npm-scaffold
    // the following is the equivalent of:
    //   npm-scaffold [yourPackageName] <args>
    execSync(`node cli.js ${TEST_PKG} ${args}`); //, { stdio: [ 0, 1, 2 ] });
}

function destroyScaffold() {
    // comment this line out if you want to see the project generated by this test (for debugging)
    execSync(`rm -rf ${TEST_PKG}`);
}

function exists(t, file, message) {
    t.ok(fs.existsSync(`${TEST_PKG}/${file}`), `${file} exists (${message})`);
}
function existsNot(t, file, message) {
    t.notOk(fs.existsSync(`${TEST_PKG}/${file}`), `${file} does not exist (${message})`);
}


function contains(t, text, expression, message) {
    t.ok(text.match(expression), `${message}: ${expression}`);
}
function containsNot(t, text, expression, message) {
    t.notOk(text.match(expression), `${message}: ${expression}`);
}

function read(file) {
    return fs.readFileSync(`${TEST_PKG}/${file}`, 'utf8');
}

function readJson(file) {
    return JSON.parse(fs.readFileSync(`${TEST_PKG}/${file}`, 'utf8'));
}
