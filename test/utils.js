const path = require('path');
const fs = require('fs');
const { execSync } = require('child_process');
// const LineByLineReader = require('line-by-line');

const TEST_PROJECT = '_test-project';

function getTestConfig() {
    return {
        project: TEST_PROJECT,
        outputDir: path.join(process.cwd(), TEST_PROJECT),
        repoIgnore: [],
        logLevel: 0
    };
}

function resetTestDir() {
    execSync(`rm -rf ${TEST_PROJECT}`);
}

function exists(t, file, message) {
    let message2 = `${file} exists`;
    if (message) {
        message2 += ` (${message})`;
    }
    t.ok(fs.existsSync(`${TEST_PROJECT}/${file}`), message2);
}

function existsNot(t, file, message) {
    let message2 = `${file} does not exist`;
    if (message) {
        message2 += ` (${message})`;
    }
    t.notOk(fs.existsSync(`${TEST_PROJECT}/${file}`), message2);
}

function read(file) {
    return fs.readFileSync(`${TEST_PROJECT}/${file}`, 'utf8');
}

function readJson(file) {
    return JSON.parse(fs.readFileSync(`${TEST_PROJECT}/${file}`, 'utf8'));
}

function contains(t, text, expression, message) {
    t.ok(text.match(expression), `${message}: \n${expression}`);
}
function containsNot(t, text, expression, message) {
    const match = text.match(expression);
    t.notOk(match, `${message}: ${expression} matches ${match}`);
}

//TODO flip this to containsDirectives for better legibility
function containsNotDirectives(t, text) {
    // directives are things like //@db //@@web //@@else //@@ and should only exist in template files
    containsNot(t, text, /\/\/@\w+/g, 'output files should not contain directives');
}


module.exports = {
    getTestConfig,
    resetTestDir,
    exists,
    existsNot,
    read,
    readJson,
    contains,
    containsNot,
    containsNotDirectives
};
