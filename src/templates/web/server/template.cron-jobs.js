//PERIODICALLY DO SOME ACTION

const cron = require('node-cron');
const fs = require('fs');
const { execSync } = require('child_process');

let counter = 0;

 //min, hour, day of month, month, day of week
 // '*/15 * * * *' = every 15 minutes
cron.schedule('*/15 * * * *', function() {
    console.log(`running cronjob (${++counter}) @ ${ new Date().toString() }...`);
  // const uploadsDir = `${__dirname}/../files`;
  // fs.readdir(uploadsDir, function(err, files) {
  //   if(err) {
  //       return console.log('cron readdir error: ' + err);
  //   }
  //   files.forEach(function(file, index) {
  //       const filePath = `${uploadsDir}/${file}`;
  //       fs.stat(filePath, function(err, stat) {
  //           if(err) {
  //               return console.log('cron stat error: ' + err);
  //           }
  //           const now = new Date().getTime();
  //           const expiry = new Date(stat.ctime).getTime(); 
  //           //3600000 = 1hr from creation
  //           if(now - expiry >= (3600000 * 2)) {
  //               console.log('  cleanup: ' + filePath);
  //               execSync('rm -rf ' + filePath);
  //               // ran into permissions error with fs.unlinkSync
  //               // fs.unlinkSync(filePath);
  //           }
  //       });
  //   });
  // });
});