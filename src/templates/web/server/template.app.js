'use strict';

//http://expressjs.com/de/api.html
const express = require('express');
const session = require('express-session');
const FileStore = require('session-file-store')(session);

const app = express();

//POST: {"name":"foo","color":"red"}  <-- JSON encoding
app.use(express.json());
//POST: name=foo&color=red            <-- URL encoding
app.use(express.urlencoded({ extended: true }));
// for remembering which user we're dealing with
app.use(session({
    name: 'server-session-cookie-id',
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true, //TODO: switch to false
    store: new FileStore({ path: 'app/web/sessions' })
}));


// serve html, js, css
app.use('/', express.static('app/web/client/'));

require('./routes/routes.js')(app);

//@db
require('./api/data.js')(app);
//@db
require('./api/users.js')(app);

//@files
require('./api/files.js')(app);



module.exports = app;




