const multer  = require('multer');
const fs = require('fs');
const path = require('path');
const { FILES_PATH } = require('../../../constants.js');

const { handleServerError } = require('./utils.js');

function api(app) {
    //https://www.npmjs.com/package/multer
    const upload = multer({ 
        storage:
            multer.diskStorage({
                //WARNING: can't access req.body parameters because multer saves the file THEN deals with writing to req (you could try and use session or move the file afterwards)
                destination: (req, file, callback) => {
                    const folderId = req.session.user || 'default';
                    const dir = `${FILES_PATH}/${folderId}`;
                    fs.mkdirSync(dir, { recursive: true });
                    callback(null, dir);
                },
                filename: (req, file, callback) => {
                    //only allow safe characters
                    const name = file.originalname.replace(/[^A-Za-z0-9-_+()\[\]{}]\./g, '').trim();
                    callback(null, name);
                }
            }),
        limits: {
            fileSize: 1e9 //1e9 = 1GB, 1e6 = 1MB
        }
    });

    // download files
    app.get('/api/files/:folderId/:fileName', (req, res) => {
        const folderId = req.params.folderId;
        const fileName = req.params.fileName;
        console.log('/files', folderId, fileName); 
        const dir = `${FILES_PATH}/${folderId}/${fileName}`;
        console.log(`downloading ${folderId}/${fileName} from ${dir}`);
        res.download(dir);
    });


    app.post('/api/files/upload', (req, res) => {
        console.log('/files/upload'); 

        // utils.checkAbility(req, res, 'upload', true, (hasAbility) => {
            // if (hasAbility) {
                //WARNING: add 'multiple' attribute to file upload input if desired 
                //upload.single('file')
                const MAX_FILES = 50;
                upload.array('files', MAX_FILES)(req, res, (err) => {

                    const tags = req.body.tags;

                    if (err instanceof multer.MulterError) {
                        // A Multer error occurred when uploading.
                        handleServerError(res, 'multer upload error', err);
                    } else if (err) {
                        // An unknown error occurred when uploading.
                        handleServerError(res, 'general upload error', err);
                    } else {

                        //req.file.destination
                        //req.file.path

                        //see multer() params if you wish to edit save paths
                        console.log(JSON.stringify(req.files, null, 4));
                        if (!req.files || req.files.length === 0) {
                            console.log('------------------');
                            console.log('no file received');
                            console.log('------------------');
                            handleServerError(res, 'error uploading file');
                        } else {
                            console.log('----------------');
                            console.log('files received');
                            console.log('----------------');
                            //TODO: sync?
                            for (let file of req.files) {
                                // const value = `${user.name}/${file.filename}`;
                                // Data.find(value, (err, result) => {
                                //     if (result && result.length >= 1) {
                                //         Data.update(result[0]._id, { value, tags });
                                //     } else {
                                //         Data.create({ value, tags });
                                //     }
                                // });
                            }

                            return res.send({ result: req.files });
                        }
                    }
                });
            // } else {
// 
            // }
        // });
    });

    app.post('/api/files/delete', (req, res) => {
        try {
            const value = req.body.value;
            console.log(`/files/delete ${value}`); 

            if (!value.startsWith(`${req.session.user}/`) && !value.startsWith('default/')) {
                return handleServerError(res, 'can only delete own files or default files', req.body);
            }

            // utils.checkAbility(req, res, 'delete', true, (hasAbility) => {
                // if (hasAbility) {
                    // const filePath = path.join(__dirname, '../../files', value);
                    const filePath = path.join('app/files', value);
                    console.log(filePath)
                    if (fs.existsSync(filePath)) {
                        console.log('  deleting file!', filePath)
                        fs.unlinkSync(filePath);
                        return res.send({ result: filePath });
                   } else {
                        handleServerError(res, 'file not found', filePath)
                    }
                // } else {
                    // console.log('TODO: permission denied (files/delete)')
                // }
            // });
        } catch (ex) {
            handleServerError(res, 'error deleting file', req.body);            
        }
    });

    app.get('/api/files/:folderId', (req, res) => {
        const folderId = req.params.folderId;
        console.log('/files', folderId);

        const dir = `${FILES_PATH}/${folderId}/`;
        const files = fs.readdirSync(dir);
        res.send({ result: { dir: folderId, files } });
    });

}

module.exports = api;
