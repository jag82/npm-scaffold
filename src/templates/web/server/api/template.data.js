const Data = require('../../../core/models/data.js');
const { checkUserLogin, handleServerError } = require('./utils.js');

function api(app) {

    // this example does not require a login to create data
    app.post('/api/createData', (req, res) => {
        try {
            const data = req.body.data;

            console.log(`/createData ${JSON.stringify(data)}`);

            Data.create(data)
                .then((result) => {
                    res.send({ result });
                })
                .catch((err) => {
                    handleServerError(res, err, data);                    
                });

        } catch (ex) {
            handleServerError(res, 'error creating data', req.body);
        }
    });

    // this example requires a login to create data
    app.post('/api/createData2', (req, res) => {
        try {
            const data = req.body.data;

            console.log(`/createData2 ${JSON.stringify(data)}`);

            checkUserLogin(
                req,
                res,
                (isLoggedIn, user) => {
                    if (isLoggedIn) {
                        Data.create(data, req.session.user)
                            .then((result) => {
                                res.send({ result });
                            })
                            .catch((err) => {
                                handleServerError(res, err, data);                    
                            });
                    }
                    else {
                        handleServerError(res, 'missing permission');
                    }
                });

        } catch (ex) {
            handleServerError(res, 'error creating data', req.body);
        }
    });   

    app.get('/api/findDataById/:id', (req, res) => {
        try {
            const id = req.params.id;

            console.log('/findDataById', id);

            Data.findById(id)
                .then((result) => {
                    res.send({ result });
                })
                .catch((err) => {
                    handleServerError(res, err, id);
                });

        } catch (ex) {
            handleServerError(res, 'error finding Data by id', req.query, ex);
        }
    });

    app.get('/api/findData', (req, res) => {
        try {
            const query = req.query.query;

            console.log(`/findData ${query}`);

            Data.find(query)
                .then((result) => {
                    res.send({ result });
                })
                .catch((err) => {
                    handleServerError(res, err, query);
                });

        } catch (ex) {
            handleServerError(res, 'error finding Data', req.query);
        }
    });

    app.post('/api/updateData', (req, res) => {
        try {
            const id = req.body.id;
            const data = req.body.data;

            console.log(`/updateData ${id} ${data}`);

            Data.update(id, data)
                .then((result) => {
                    res.send({ result });
                })
                .catch((err) => {
                    handleServerError(res, err, id);
                });

        } catch (ex) {
            handleServerError(res, 'error updating data', req.body);
        }
    });

    app.post('/api/deleteData', (req, res) => {
        try {
            const id = req.body.id;

            console.log(`/deleteData ${id}`);

            // TODO: only allow owners to delete
            // if (!value.startsWith(`${req.session.user}/`)) {
            //     return handleServerError(res, 'can only delete own data', req.body);
            // }

            // utils.checkAbility(req, res, 'delete', true, (hasAbility) => {
                // if (hasAbility) {
                    Data.delete(id)
                        .then((result) => {
                            res.send({ result });                
                        })
                        .catch((err) => {
                            handleServerError(res, err, id);                            
                        })

                // } else {
                    // console.log('TODO: data permission missing')
                // }
            // });
        } catch (ex) {
            handleServerError(res, 'error deleting data', req.body);
        }
    });

    app.post('/api/resetData', (req, res) => {
        try {
            console.log('/resetData');

            // TODO: only allow admins to reset

            Data.reset()
                .then((result) => {
                    res.send({ result });                
                })
                .catch((err) => {
                    handleServerError(res, err, id);                    
                });

        } catch (ex) {
            handleServerError(res, 'error resetting data', req.body);
        }
    });
}

module.exports = api;
