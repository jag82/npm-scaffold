const Users = require('../../../core/models/users.js');

// the simplest middle ware, can be passed as second parameter to a route
// app.get('/some-route', checkAuth, (req, res) => { ... })
function checkAuth(req, res, next) {
    if (req.session.user === 'admin@admin.admin') {
        next();
    } else {
        handleServerError(res, 'unauthorized');
    }
}

// check(user) returns a boolean (e.g. is logged in? has ability?, is owner of data?)
async function checkUser(req, res, check, callback) {
    try {
        console.log('checking user...');

        Users.findOne(req.session.user, (err, user) => {
            if (err) {
                handleServerError(res, err, req.session.user);
            } else {
                const result = check(user); 
                return callback(result, user);
            }
        });
    } catch (ex) {
        console.log(`error: ${ex}`);
    }    
}

async function checkUserLogin(req, res, callback) {
    return checkUser(
        req,
        res,
        (user) => {
            return user !== null;
        },
        callback
    );
}


async function checkUserAbility(req, res, ability, value, callback) {
    return checkUser(
        req,
        res,
        (user) => {
            if (typeof user !== 'object' || typeof user.abilities !== 'object') {
                console.log('  invalid user/abilities type');
                return false;
            }

            if (!ability) {
                return true;
            }

            const userAbility = user.abilities[ability] || [];

            if (typeof userAbility === 'boolean') {
                console.log(`  ability = ${userAbility}`);
                return userAbility;
            } else {
                console.log(`  ability = ${userAbility}`);
                return userAbility.includes(value);
            }            
        },
        callback
   );  
}


function handleServerError(res, error, data) {
    if (error) {
        try {
            res.send({ error, data });
        } catch (ex) {
            console.log(`error: ${ex}`);
        }        
    }
}

module.exports = {
    checkUser,
    checkUserLogin,
    checkUserAbility,
    handleServerError
};
    
