const Users = require('../../../core/models/users.js');
const { handleServerError } = require('./utils.js');

function api(app) {

    app.post('/api/createUser', (req, res) => {
        try {
            const { name, password } = req.body;
            console.log(`/createUser ${name}`);

            // utils.checkAbility(req, res, 'users', true, (hasAbility) => {
                // if (hasAbility) {
                    Users.create({ name, password }, (err, result) => {
                        if (err) {
                            handleServerError(res, err, name);
                        } else {
                            res.send({ result });
                        }
                    });
                // } else {
                    // console.log('TODO: missing users permission');
                // }
            // });
        } catch (ex) {
            handleServerError(res, 'error creating user', req.body.name);
        }
    });

    app.post('/api/updateUser', (req, res) => {
        try {
            const id = req.body.id;
            const user = req.body.user;
            console.log(`/updateUser ${id}`);

            // utils.checkAbility(req, res, 'users', true, (hasAbility) => {
                // if (hasAbility) {
                    Users.update(id, user, (err, result) => {
                        if (err) {
                            handleServerError(res, err, id);
                        } else {
                            res.send({ result });                
                        }
                    });
                // } else {
                    // console.log('TODO: missing users permission');
                // }
            // });
        } catch (ex) {
            handleServerError(res, 'error deleting data', req.body);
        }
    });

    app.post('/api/deleteUser', (req, res) => {
        try {
            const id = req.body.id;
            console.log(`/deleteUser ${id}`);

            // utils.checkAbility(req, res, 'users', true, (hasAbility) => {
                // if (hasAbility) {
                    Users.delete(id, (err, result) => {
                        if (err) {
                            handleServerError(res, err, name);
                        } else {
                            res.send({ result });
                        }
                    });
                // } else {
                    // console.log('TODO: missing users permission');
                // }
            // });
        } catch (ex) {
            handleServerError(res, 'error deleting user', req.body);
        }
    });


    app.post('/api/login', (req, res) => {
        try {
            const { name, password } = req.body;
            console.log(`/login ${name}`);

            Users.login(name, password, (err, result) => {
                if (err) {
                    handleServerError(res, err, name);
                } else {
                    req.session.user = name;
                    res.send({ result });
                }
            });
        } catch (ex) {
            handleServerError(res, 'error logging in', req.body.name);
        }
    });

    app.get('/api/logout', (req, res) => {
        try {
            console.log('/logout');
            console.log(`  ${req.session.user}`);
            let result;
            if (req.session.user) {
                result = req.session.user;
                req.session.user = null;
            } else {
                result = false;
            }
            res.send({ result });
        } catch (ex) {
            handleServerError(res, 'error logging out');
        }
    });

    app.get('/api/getCurrentUser', (req, res) => {
        try {
            console.log('/getCurrentUser');
            res.send({ result: req.session.user });
        } catch (ex) {
            handleServerError(res, 'error getting current user');
        }
    });
}

module.exports = api;
