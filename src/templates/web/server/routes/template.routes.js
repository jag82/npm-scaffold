// const fs = require('fs');
const path = require('path');
const CLIENT_ROOT_DIR = path.join(__dirname, '../../client');

function routes(app) {
    // main page
    app.get('/', (req, res) => {
        res.sendFile('index.html', { root: CLIENT_ROOT_DIR });
    });

    app.get('/login', (req, res) => {
        res.sendFile('pages/login/login.html', { root: CLIENT_ROOT_DIR });
    });

    // create
    app.get('/data/create', requireLogin, (req, res) => {
        res.sendFile('pages/create/create.html', { root: CLIENT_ROOT_DIR });
    });

    // list
    app.get('/data/list', (req, res) => {
        res.sendFile('pages/list/list.html', { root: CLIENT_ROOT_DIR });
    });

    // view one
    app.get('/data/:id', (req, res) => {
        res.sendFile('pages/view/view.html', { root: CLIENT_ROOT_DIR });
    });

    // edit
    app.get('/data/edit/:id', (req, res) => {
        res.sendFile('pages/edit/edit.html', { root: CLIENT_ROOT_DIR });
    });
}

// usage:
// app.get('/some-route', requireLogin, (req, res) => {} );
function requireLogin(req, res, next) {
    console.log(`checking auth for ${req.url}`);
    console.log(`  ${req.session.user}`);
    const isAuthorized = req.session.user;
    if (isAuthorized) {
        return next();
    } else {
        res.redirect(`/login?redirect=${req.url}`);
    }
}

module.exports = routes;
