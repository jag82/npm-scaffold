// direct console.log statements are to communicate how things work
// they should be removed and/or replaced with real code
$(document).ready(function() {

    // STARTUP CODE
    console.log('index.js running...');

    console.log('getting current user...');
    $.get('/api/getCurrentUser', function(response) {
        status('...got current user');
        if (response.error) {
            handleError(response, 'getCurrentUser error', name);
        }
        loginStatus(response.result);
        console.log(response.result);
    });

    // EVENT HANDLERS
//@@db
    $(document).on('click', '[data-action="create"]', function(event) {
        const value = $('[name="value"]').val();
        createData(value);
    });

    $(document).on('click', '[data-action="create2"]', function(event) {
        const value = $('[name="value2"]').val();
        createData2(value);
    });

    $(document).on('click', '[data-action="search"]', function(event) {
        const query = $('[name="query"]').val();
        findData(query);
    });

    $(document).on('click', '[data-action="update"]', function(event) {
        const target = $(event.currentTarget).closest('[data-id]');
        const id = target.data('id');
        const update = { value: 'new data', tags: [ 'juicy' ] };
        updateData(id, update);
    });

    $(document).on('click', '[data-action="delete"]', function(event) {
        const target = $(event.currentTarget).closest('[data-id]');
        const id = target.data('id');
        deleteData(id);
    });

    $(document).on('click', '[data-action="reset"]', function(event) {
        resetData();
    });

    $(document).on('click', '[data-action="createUser"]', function(event) {
        const name = $('.createUser [name="name"]').val();
        const password = $('.createUser [name="password"]').val();
        createUser(name, password);
    });

    $(document).on('click', '[data-action="login"]', function(event) {
        const name = $('.login [name="name"]').val();
        const password = $('.login [name="password"]').val();
        login(name, password);
    });

    $(document).on('click', '[data-action="logout"]', function(event) {
        logout();
    });
//@@

//@@files
    $(document).on('click', '[data-action="download"]', function(event) {
        const target = $(event.currentTarget);
        const link = target.data('`link');
        download(link);
    });

    $(document).on('click', '[data-action="upload"]', function(event) {
        const submitElement = $(event.currentTarget);
        const fileElement = submitElement.siblings('input[type="file"]');
        const namesElement = submitElement.siblings('input[name="names"]');
        const progressElement = submitElement.siblings('.progress');
        upload(fileElement, namesElement.val(), progressElement);
    });

    $(document).on('click', '[data-action="deleteFile"]', function(event) {
        const target = $(event.currentTarget).closest('[data-value]');
        const value = target.data('value');
        deleteFile(value);
    });
//@@

    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    // FUNCTIONS
//@@db
    function createData(data) {
        status(`creating data ${data}...`);

        $.post('/api/createData', { data }, function(response) {
            status('...created data');
            if (response.error) {
                handleError(response, 'createData error', data);
            }
            $('.create .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }

    function createData2(data) {
        status(`creating data 2 ${data}...`);

        $.post('/api/createData2', { data }, function(response) {
            status('...created data 2');
            if (response.error) {
                handleError(response, 'createData2 error', data);
            }
            $('.create2 .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }

    function findData(query) {
        status(`finding data ${query}...`);
        $('.find .results').html('');

        $.get('/api/findData', {
            query
        }, function(response) {
            status('...found data');
            if (response.error) {
                handleError(response, 'findData error', query);
            } else {
                $('.find .results').append(`<div>${response.result.length} results</div><hr>`);
                
                for (let data of response.result) {
                    $('.find .results').append(`
                        <div data-id="${data._id}">
                            <div><b class="value">${data.value}</b></div>
                            <div><small><i class="tags">${data.tags.join(' ')}</i></small></div>
                            <div><small><b class="users">${data.users.join(' ')}</b></small></div>
                            <div><small class="date">${data.date.toString()}</small></div>
                            <div><small><span class="author">${data.author || ''}</span></small></div>
                            <button data-action="delete">X</button>
                            <button data-action="update">update</button>
                        </div><hr>
                    `);
                }
            }
            console.log(response.result);
        });
    }

    function updateData(id, data) {
        status(`updating data ${id} ${data}...`);

        $.post('/api/updateData', {
            id,
            data
        }, function(response) {
            status('...updated data');
            if (response.error) {
                handleError(response, 'updateData error', { id, data });
            } else {
                const updatedData = $(`.find .results [data-id="${id}"]`);
                updatedData.find('.value').html(data.value);
                updatedData.find('.tags').html(data.tags.join(' '));
            }
            console.log(response.result);
        });
    }

    function deleteData(id) {
        status(`deleting data ${id}...`);

        $.post('/api/deleteData', {
            id
        }, function(response) {
            status('...deleted data');
            if (response.error) {
                handleError(response, 'deleteData error', id);
            } else {
                $(`.find .results [data-id="${id}"]`).remove();
            }
            console.log(response.result);
        });
    }

    function resetData() {
        status('resetting data...');

        $.post('/api/resetData', {
        
        }, function(response) {
            status('...resetted data');
            if (response.error) {
                handleError(response, 'resetData error', id);
            }
            $('.reset .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }
    
    function createUser(name, password) {
        status(`creating user ${name}...`);

        $.post('/api/createUser', { name, password }, function(response) {
            status('...created user');
            if (response.error) {
                handleError(response, 'createUser error', name);
            }
            $('.createUser .results').html(JSON.stringify(response, null, 4));                
            console.log(response);
        });
    }

    function login(name, password) {
        status(`user logging in ${name}...`);

        $.post('/api/login', {
            name,
            password
        }, function(response) {
            status('...logged in user');
            if (response.error) {
                handleError(response, 'user login error', name);
            }
            $('.login .results').html(JSON.stringify(response, null, 4));                
            loginStatus(response.result.name);
            console.log(response.result);
        });
    }

    function logout() {
        status('user logging out...');

        $.get('/api/logout', function(response) {
            status('...logged out user');
            if (response.error) {
                handleError(response, 'user logout error');
            }
            $('.logout .results').html(JSON.stringify(response, null, 4));                
            loginStatus();
            console.log(response.result);
            window.location.replace('/');
        });
    }
//@@

//@@files
    function download(link) {
        window.open(link, '_self');
    }

    function upload(fileElement, fileName, progressElement) {
        status('uploading...');
        progressElement.html('');

        fileName = fileName.replace(/[^A-Za-z0-9-_+()\[\]{}]/g, '').trim();
        const formData = new FormData();

        let file;
        let customName;
        let ext;
        for (let i = 0; i < fileElement[0].files.length; i++) {
            file = fileElement[0].files[i];

            //custom name for file?
            if (fileName) {
                ext = file.name.split('.').pop();
                if (i === 0) {
                    customName = `${fileName}.${ext}`;
                } else {
                    customName = `${fileName}-${i+1}.${ext}`;
                }
            } else {
                customName = file.name; //use original filename
            }

            formData.append(fileElement.attr('name'), file, customName); 
        }

        progressElement.html('uploading...');

        $.ajax({
            type: 'POST',
            url: '/api/files/upload',
            data: formData,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            cache: false,
            // custom xhr allows us to show upload progress
            xhr: function() {
                const xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener('progress', function(event) {
                    if (event.lengthComputable) {
                        const progress = parseInt(event.loaded / event.total * 100, 10);
                        progressElement.html(`${progress}%`);
                        if (progress === 100) {

                            const baseFilesUrl = `/api/files/${window.currentUser || 'default'}`;

                            progressElement.html(`
                                ...upload complete
                                <ul>
                                    <li>
                                        <img src="${baseFilesUrl}/${customName}" alt="the file you just uploaded">
                                    </li>
                                    <li>
                                        <a href="${baseFilesUrl}/${customName}">download this file</a>
                                    </li>
                                    <li>
                                        <div data-action="deleteFile" data-value="${window.currentUser || 'default'}/${customName}">delete this file</div>
                                    </li>
                                    <li>
                                        <a href="${baseFilesUrl}">list of files (json)</a>
                                    </li>
                                </ul>
                            `);
                        }
                    }
                }, false);

                return xhr;
            },
            success: function(response) {
                status(`...upload complete (should appear in app/files/${window.currentUser || 'default'}/)`);
                if (response.error) {
                    handleError(response, 'upload error', response);
                }
                console.log(response);
            }
        });        
    }

    function deleteFile(value) {
        status(`deleting file ${value}...`);

        $.post('/api/files/delete', { value }, function(response) {
            status(`...deleted file ${value}`);
            if (response.error) {
                handleError(response, 'delete file error');
            }
            console.log(response.result);
        });
    }

//@@

    // HELPERS
    // ADVANCED FEATURES + DEBUGGING
    // click '#advanced' many times to activate
    let counter = 0;
    let prev = 0;
    let advanced = false;

    $(document).on('click', '#advanced', function(event) {
        if (advanced) {
            return;
        }

        const now = new Date().getTime();
        if (now - prev > 1000) {
            counter = 0;
        } 
        counter++;
        prev = now;
        
        const clicksForAdvancedMode = 5;
        console.log(`keep clicking for advanced mode (${clicksForAdvancedMode - counter})`);
        if (counter >= clicksForAdvancedMode) {
            advanced = true;
            $('body').addClass('advanced');
            $('#advanced').html('advanced mode unlocked!');
        }
    });

    function loginStatus(username) {
        window.currentUser = username;
        $('#loginStatus').html(username ? `logged in as ${username}` : 'not logged in');
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
