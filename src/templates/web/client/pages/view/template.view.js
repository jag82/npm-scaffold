// direct console.log statements are to communicate how things work
// they should be removed and/or replaced with real code
$(document).ready(function() {

    // STARTUP CODE
    console.log('view.js running...');

    console.log('getting current user...');
    $.get('/api/getCurrentUser', function(response) {
        status('...got current user');
        if (response.error) {
            handleError(response, 'getCurrentUser error', name);
        }
        loginStatus(response.result);
        console.log(response.result);
    });

    // get id of data this page should be for
    const parts = window.location.href.split('/');
    const id = parts.pop() || parts.pop(); // handle potential trailing slash
    console.log('getting data...', id);

    $.get(`/api/findDataById/${id}`, function(response) {
        status('...got data by id');
        if (response.error) {
            handleError(response, 'findDataById error', id);
        }
        const data = response.result;

        $('.result').data('id', data._id);
        $('.value').html(data.value);
        $('.tags').html(data.tags);
        $('.users').html(data.users);
        $('.date').html(data.date);
        $('.author').html(data.author);
        // $('.image').attr('src', `/api/files/${data.author}/${data._id}.png`);

        console.log(response.result);
    });

    // EVENT HANDLERS
//@@db

//@@

//@@files

//@@
    $(document).on('click', '[data-action="edit"]', function(event) {
        const id = $('.result').data('id');
        console.log('editing ', id);
        window.location = `/data/edit/${id}`;
    });


    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    // FUNCTIONS
//@@db

//@@

//@@files

//@@

    // HELPERS
    // ADVANCED FEATURES + DEBUGGING
    // click '#advanced' many times to activate
    let counter = 0;
    let prev = 0;
    let advanced = false;

    $(document).on('click', '#advanced', function(event) {
        if (advanced) {
            return;
        }

        const now = new Date().getTime();
        if (now - prev > 1000) {
            counter = 0;
        } 
        counter++;
        prev = now;
        
        const clicksForAdvancedMode = 5;
        console.log(`keep clicking for advanced mode (${clicksForAdvancedMode - counter})`);
        if (counter >= clicksForAdvancedMode) {
            advanced = true;
            $('body').addClass('advanced');
            $('#advanced').html('advanced mode unlocked!');
        }
    });

    function loginStatus(username) {
        $('#loginStatus').html(username ? `logged in as ${username}` : 'not logged in');
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
