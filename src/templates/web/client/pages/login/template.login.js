$(document).ready(function() {

    console.log('login.js running...');

    // gets query params from url (for ?redirect=/some-url)
    const params = new URLSearchParams(window.location.search);

    $(document).on('click', '[data-action="login"]', function(event) {
        const name = $('.login [name="name"]').val();
        const password = $('.login [name="password"]').val();
        login(name, password);
    });

    $(document).on('click', '[data-action="logout"]', function(event) {
        logout();
    });


    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    function login(name, password) {
        // /login?redirect=/
        const redirect = params.get('redirect');
        status(`user logging in ${name}... ${redirect}`);

        $.post('/api/login', {
            name,
            password
        }, function(response) {
            status('...logged in user');
            if (response.error) {
                handleError(response, 'user login error', name);
            }
            $('.login .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
            if (redirect) {
                window.location.replace(redirect);
            }
        });
    }

    function logout() {
        status('user logging out...');

        $.get('/api/logout', function(response) {
            status('...logged out user');
            if (response.error) {
                handleError(response, 'user logout error');
            }
            $('.logout .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
