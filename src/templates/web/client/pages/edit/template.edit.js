// direct console.log statements are to communicate how things work
// they should be removed and/or replaced with real code
$(document).ready(function() {

    // STARTUP CODE
    console.log('edit.js running...');

    console.log('getting current user...');
    $.get('/api/getCurrentUser', function(response) {
        status('...got current user');
        if (response.error) {
            handleError(response, 'getCurrentUser error', name);
        }
        loginStatus(response.result);
        console.log(response.result);
    });

    // get id of data this page should be for
    const parts = window.location.href.split('/');
    const id = parts.pop() || parts.pop(); // handle potential trailing slash
    console.log('getting data...', id);

    $.get(`/api/findDataById/${id}`, function(response) {
        status('...got data by id');
        if (response.error) {
            handleError(response, 'findDataById error', id);
        }
        const data = response.result;

        $('.result').data('id', data._id);
        $('[name="value"]').val(data.value);
        // TODO: if files:        
        // $('.image').attr('src', `/api/files/${idea.author}/${idea._id}.png`);

        console.log(response.result);
    });

    // EVENT HANDLERS
//@@db
    $(document).on('click', '[data-action="update"]', function(event) {
        const id = $('.result').data('id');

        const data = {};
        data.value = $('[name="value"]').val();

        updateData(id, data);
    });

    $(document).on('click', '[data-action="view"]', function(event) {
        const id = $('.result').data('id');
        console.log('viewing ', id);
        window.location = `/data/${id}`;
    });

    $(document).on('click', '[data-action="delete"]', function(event) {
        const id = $('.result').data('id');
        deleteData(id);
    });
//@@

//@@files
    $(document).on('click', '[data-action="upload"]', function(event) {
        const submitElement = $(event.currentTarget);
        const fileElement = submitElement.siblings('input[type="file"]');
        const namesElement = submitElement.siblings('input[name="names"]');
        const progressElement = submitElement.siblings('.progress');
        upload(fileElement, namesElement.val(), progressElement);
    });
//@@

    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    // FUNCTIONS
//@@db
    function updateData(id, data) {
        status(`updating data ${id} ${data}...`);

        $.post('/api/updateData', { id, data }, function(response) {
            status('...created data');
            if (response.error) {
                handleError(response, 'updateData error', { id, data });
            } else {
                $('.update .results').html(JSON.stringify(response, null, 4));                
            }
            console.log(response.result);
        });
    }


    function deleteData(id) {
        status(`deleting data ${id}...`);

        $.post('/api/deleteData', {
            id
        }, function(response) {
            status('...deleted data');
            if (response.error) {
                handleError(response, 'deleteData error', id);
            } else {
                $(`.find .results [data-id="${id}"]`).remove();
            }
            console.log(response.result);
        });
    }
//@@

//@@files
    function upload(fileElement, fileName, progressElement) {
        status('uploading...');
        progressElement.html('');

        fileName = fileName.replace(/[^A-Za-z0-9-_+()\[\]{}]/g, '').trim();
        const formData = new FormData();

        let file;
        let customName;
        let ext;
        for (let i = 0; i < fileElement[0].files.length; i++) {
            file = fileElement[0].files[i];

            //custom name for file?
            if (fileName) {
                ext = file.name.split('.').pop();
                if (i === 0) {
                    customName = `${fileName}.${ext}`;
                } else {
                    customName = `${fileName}-${i+1}.${ext}`;
                }
            } else {
                customName = file.name; //use original filename
            }

            formData.append(fileElement.attr('name'), file, customName); 
        }

        progressElement.html('uploading...');

        $.ajax({
            type: 'POST',
            url: '/api/files/upload',
            data: formData,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            cache: false,
            // custom xhr allows us to show upload progress
            xhr: function() {
                const xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener('progress', function(event) {
                    if (event.lengthComputable) {
                        const progress = parseInt(event.loaded / event.total * 100, 10);
                        progressElement.html(`${progress}%`);
                
                        if (progress === 100) {
                            progressElement.html('...upload complete (should appear in ${FILES_PATH}/default/)');
                        }
                    }
                }, false);

                return xhr;
            },
            success: function(response) {
                status('...upload complete (should appear in ${FILES_PATH}/default/)');
                if (response.error) {
                    handleError(response, 'upload error', response);
                }
                console.log(response);
            }
        });        
    }
//@@

    // HELPERS
    // ADVANCED FEATURES + DEBUGGING
    // click '#advanced' many times to activate
    let counter = 0;
    let prev = 0;
    let advanced = false;

    $(document).on('click', '#advanced', function(event) {
        if (advanced) {
            return;
        }

        const now = new Date().getTime();
        if (now - prev > 1000) {
            counter = 0;
        } 
        counter++;
        prev = now;
        
        const clicksForAdvancedMode = 5;
        console.log(`keep clicking for advanced mode (${clicksForAdvancedMode - counter})`);
        if (counter >= clicksForAdvancedMode) {
            advanced = true;
            $('body').addClass('advanced');
            $('#advanced').html('advanced mode unlocked!');
        }
    });

    function loginStatus(username) {
        $('#loginStatus').html(username ? `logged in as ${username}` : 'not logged in');
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
