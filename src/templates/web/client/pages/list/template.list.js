// direct console.log statements are to communicate how things work
// they should be removed and/or replaced with real code
$(document).ready(function() {

    // STARTUP CODE
    console.log('list.js running...');

    console.log('getting current user...');
    $.get('/api/getCurrentUser', function(response) {
        status('...got current user');
        if (response.error) {
            handleError(response, 'getCurrentUser error', name);
        }
        loginStatus(response.result);
        console.log(response.result);
    });

    findData();

    // EVENT HANDLERS
//@@db
    $(document).on('click', '[data-action="search"]', function(event) {
        const query = $('[name="query"]').val();
        findData(query);
    });

//@@

//@@files
    $(document).on('click', '[data-action="download"]', function(event) {
        const target = $(event.currentTarget);
        const link = target.data('link');
        download(link);
    });

    $(document).on('click', '[data-action="upload"]', function(event) {
        const submitElement = $(event.currentTarget);
        const fileElement = submitElement.siblings('input[type="file"]');
        const namesElement = submitElement.siblings('input[name="names"]');
        const progressElement = submitElement.siblings('.progress');
        upload(fileElement, namesElement.val(), progressElement);
    });
//@@

    $(document).on('click', '[data-action="view"]', function(event) {
        const target = $(event.currentTarget);
        const id = target.closest('[data-id]').data('id');
        console.log('viewing ', id);
        window.location = `/data/${id}`;
    });

    $(document).on('click', '[data-action="edit"]', function(event) {
        const target = $(event.currentTarget);
        const id = target.closest('[data-id]').data('id');
        console.log('editing ', id);
        window.location = `/data/edit/${id}`;
    });

    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    // FUNCTIONS
//@@db
    function findData(query) {
        status(`finding data ${query}...`);
        $('.find .results').html('');

        $.get('/api/findData', {
            query
        }, function(response) {
            status('...found data');
            if (response.error) {
                handleError(response, 'findData error', query);
            } else {
                $('.find .results').append(`<div>${response.result.length} results</div><hr>`);
                
                for (let data of response.result) {
                    $('.find .results').append(`
                        <div data-id="${data._id}">
                            <div><b class="value">${data.value}</b></div>
                            <div><small><i class="tags">${data.tags.join(' ')}</i></small></div>
                            <div><small><b class="users">${data.users.join(' ')}</b></small></div>
                            <div><small class="data">${data.date.toString()}</small></div>
                            <div><small><span class="author">${data.author || ''}</span></small></div>
                            <button data-action="view">view</button>
                            <button data-action="edit">edit</button>
                        </div><hr>
                    `);
                }
            }
            console.log(response.result);
        });
    }
//@@

//@@files
    function download(link) {
        window.open(link, '_self');
    }

    function upload(fileElement, fileName, progressElement) {
        status('uploading...');
        progressElement.html('');

        fileName = fileName.replace(/[^A-Za-z0-9-_+()\[\]{}]/g, '').trim();
        const formData = new FormData();

        let file;
        let customName;
        let ext;
        for (let i = 0; i < fileElement[0].files.length; i++) {
            file = fileElement[0].files[i];

            //custom name for file?
            if (fileName) {
                ext = file.name.split('.').pop();
                if (i === 0) {
                    customName = `${fileName}.${ext}`;
                } else {
                    customName = `${fileName}-${i+1}.${ext}`;
                }
            } else {
                customName = file.name; //use original filename
            }

            formData.append(fileElement.attr('name'), file, customName); 
        }

        progressElement.html('uploading...');

        $.ajax({
            type: 'POST',
            url: '/api/files/upload',
            data: formData,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            cache: false,
            // custom xhr allows us to show upload progress
            xhr: function() {
                const xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener('progress', function(event) {
                    if (event.lengthComputable) {
                        const progress = parseInt(event.loaded / event.total * 100, 10);
                        progressElement.html(`${progress}%`);
                
                        if (progress === 100) {
                            progressElement.html('...upload complete (should appear in ${FILES_PATH}/default/)');
                        }
                    }
                }, false);

                return xhr;
            },
            success: function(response) {
                status('...upload complete (should appear in ${FILES_PATH}/default/)');
                if (response.error) {
                    handleError(response, 'upload error', response);
                }
                console.log(response);
            }
        });        
    }
//@@

    // HELPERS
    // ADVANCED FEATURES + DEBUGGING
    // click '#advanced' many times to activate
    let counter = 0;
    let prev = 0;
    let advanced = false;

    $(document).on('click', '#advanced', function(event) {
        if (advanced) {
            return;
        }

        const now = new Date().getTime();
        if (now - prev > 1000) {
            counter = 0;
        } 
        counter++;
        prev = now;
        
        const clicksForAdvancedMode = 5;
        console.log(`keep clicking for advanced mode (${clicksForAdvancedMode - counter})`);
        if (counter >= clicksForAdvancedMode) {
            advanced = true;
            $('body').addClass('advanced');
            $('#advanced').html('advanced mode unlocked!');
        }
    });

    function loginStatus(username) {
        $('#loginStatus').html(username ? `logged in as ${username}` : 'not logged in');
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
