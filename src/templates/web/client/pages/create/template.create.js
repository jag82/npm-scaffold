// direct console.log statements are to communicate how things work
// they should be removed and/or replaced with real code
$(document).ready(function() {

    // STARTUP CODE
    console.log('create.js running...');

    console.log('getting current user...');
    $.get('/api/getCurrentUser', function(response) {
        status('...got current user');
        if (response.error) {
            handleError(response, 'getCurrentUser error', name);
        }
        loginStatus(response.result);
        console.log(response.result);
    });

    // EVENT HANDLERS
//@@db
    $(document).on('click', '[data-action="create"]', function(event) {
        const value = $('[name="value"]').val();
        createData(value);
    });

    $(document).on('click', '[data-action="create2"]', function(event) {
        const value = $('[name="value2"]').val();
        createData2(value);
    });

    $(document).on('click', '[data-action="reset"]', function(event) {
        resetData();
    });
//@@

    $(document).on('keyup', function(event) {
        if (event.keyCode === 13) {
            console.log('enter pressed - clicking nearby data-action element');
            $(event.target).siblings('[data-action]').click();
        }
    });


    // FUNCTIONS
//@@db
    function createData(data) {
        status(`creating data ${data}...`);

        $.post('/api/createData', { data }, function(response) {
            status('...created data');
            if (response.error) {
                handleError(response, 'createData error', data);
            }
            $('.create .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }

    function createData2(data) {
        status(`creating data 2 ${data}...`);

        $.post('/api/createData2', { data }, function(response) {
            status('...created data 2');
            if (response.error) {
                handleError(response, 'createData2 error', data);
            }
            $('.create2 .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }

    function resetData() {
        status('resetting data...');

        $.post('/api/resetData', {
        
        }, function(response) {
            status('...resetted data');
            if (response.error) {
                handleError(response, 'resetData error', id);
            }
            $('.reset .results').html(JSON.stringify(response, null, 4));                
            console.log(response.result);
        });
    }
//@@

    // HELPERS
    // ADVANCED FEATURES + DEBUGGING
    // click '#advanced' many times to activate
    let counter = 0;
    let prev = 0;
    let advanced = false;

    $(document).on('click', '#advanced', function(event) {
        if (advanced) {
            return;
        }

        const now = new Date().getTime();
        if (now - prev > 1000) {
            counter = 0;
        } 
        counter++;
        prev = now;
        
        const clicksForAdvancedMode = 5;
        console.log(`keep clicking for advanced mode (${clicksForAdvancedMode - counter})`);
        if (counter >= clicksForAdvancedMode) {
            advanced = true;
            $('body').addClass('advanced');
            $('#advanced').html('advanced mode unlocked!');
        }
    });

    function loginStatus(username) {
        $('#loginStatus').html(username ? `logged in as ${username}` : 'not logged in');
    }

    function status(msg) {
        $('#status').html(msg || '');
        console.log(msg);
    }

    function handleError(response, message, data) {
        try {
            status(`
                Server error: 
                ${response.error} 

                Client message:
                ${message}

                Data:
                ${JSON.stringify(data, null, 4)}
            `);
        } catch (ex) {
            console.log(message);
        }
    }

});
