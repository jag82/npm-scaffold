//@@settings
const settings = require('../settings.json');
const PORT = settings.port || process.argv[2];
//@@else
const PORT = process.argv[2] || 3000;
//@@

//@db
const DB_URI = `${settings.mongoUri}/${PROJECT_NAME}`; 
// TODO add dbpath?

const app = require('./web/server/app.js');
// wait for mongo connection before starting app:
// https://blog.cloudboost.io/waiting-for-db-connections-before-app-listen-in-node-f568af8b9ec9
app.on('ready', function() { 
    app.listen(PORT, function() {
        console.log(`app listening @ localhost:${PORT} ...`);
    });

    require('./web/server/cron-jobs.js');
});

//@@db
const db = require('./core/db.js');

db.connect(DB_URI, function() {
    console.log(`db connected @ ${DB_URI} ...`);
    app.emit('ready');
});
//@@else
app.emit('ready');
//@@