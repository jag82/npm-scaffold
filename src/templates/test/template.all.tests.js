const test = require('tape-async'); //allows us to report thrown exceptions as test failures

// ------------------------------------------
// IF YOU NEED TO THE DB FOR YOUR TESTS...
// ------------------------------------------
// const settings = require('../settings.json');
// const DB_URI = `${settings.mongoUri}/split`; //mongodb://127.0.0.1:27017/databaseName
// TODO add dbpath too?
// const db = require('../core/db.js');

// test('connect to db for all tests...', (t) => {
//     db.connect(DB_URI, () => {
//         t.end();
//     });
// });

// test.onFinish(() => {
//     db.close();
// })

// // write tests that use the db here
// // e.g. if Budget.set/get return a Promise:
// const Budget = require('../core/models/budget.js');

// test('can add budget', async function(t) {
//     await Budget.set({ name: 'jag',  amount: 100 })
//         .then(() => {
//             return Budget.get({ name: 'jag' });  
//         })
//         .then((budget) => {
//             t.equal(budget.name, 'jag');
//             t.equal(budget.amount, 100);
//         })
// });
// ------------------------------------------


// true assertions will result in a passing test
test('positive control', function(t){
    t.equal(1, 1);
    t.deepEqual({ a: { b: 2 } }, { a: { b: 2 } });
    t.end();    
});

// false assertions result in a failing test, which we are explicitly skipping
test.skip('negative control', function(t) {
    t.equal(0, 1);
    t.end();
});

// asynchronous tests are easy, just plan the number of expected assertions beforehand
test('async test', function(t) {
    t.plan(2);
    t.equal(true, true);
    setTimeout(function() {
        t.equal(true, true);
    }, 1000);
});

// code that throws an exception results in a test failure
test.skip('throw an exception test', function(t) {
    asdf 
});
