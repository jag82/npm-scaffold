const mongoose = require('mongoose');
 
const Model = mongoose.model('Data', new mongoose.Schema({
    id: mongoose.Schema.ObjectId,
    value: String,
    tags: [ String ],
    users: [ String ],
    date: Date,
    author: String
}), 'data');

const Data = {};

// CREATE
Data.create = function(dataOrString, author) {
    const data = normalizeData(dataOrString);
    data.date = new Date();
    if (author) {
        data.author = author;
    }

    console.log(`creating data: ${JSON.stringify(data)}`);

    return new Promise((resolve, reject) => {
        Model.create(data, (err) => {
            if (err) { 
                console.info('Data created error', err);
                return reject(err);
            } else {
                console.info('Data created', data);
                return resolve(data);
            }
        });
    });
};

// READ
Data.findById = function(id) {
    console.log(`finding Data by id: ${id}`);
    
    return new Promise((resolve, reject) => {
        Model.findById(id, (err, result) => {
            if (err) { 
                console.info('Data found by id error', err);
                return reject(err);
            } else {
                console.info('Data found by id', result);
                return resolve(result);
            }
        });
    });
};

// can search by #tags @users and regular words too
Data.find = function(queryString = '') {
    const query = { $or: [] };
    const search = parseString(queryString);

    // case-insensitive inexact matches
    const regex = new RegExp(search.value, 'i');
    query.$or.push({ value: { $regex: regex } });

    if (search.tags.length > 0) {
        query.$or.push({ tags: { $all: search.tags } });
    }
    if (search.users.length > 0) {
        query.$or.push({ users: { $all: search.users } });
    }

    console.log(`finding data: ${JSON.stringify(query, null, 4)}`);
    
    return new Promise((resolve, reject) => {
        Model.find(query, (err, result) => {
            if (err) { 
                console.info('Data found error', err);
                return reject(err);
            } else {
                console.info('Data found', result);
                return resolve(result);
            }
        });
    });
};

// UPDATE
Data.update = function(id, update) {
    console.log(`updating data: ${id} ${JSON.stringify(update, null, 4)}`);

    return new Promise((resolve, reject) => {
        Model.updateOne({ _id: id }, update, (err, result) => {
            if (err) { 
                console.info('Data updated error', err);
                return reject(err);
            } else {
                //result: { n: 1, nModified: 1, ok: 1 }
                console.info('Data updated', id);
                return resolve(id);
            }
        });
    });
};

// UPSERT
Data.upsert = function(query, dataOrString) {
    const data = normalizeData(dataOrString);
    data.date = new Date();

    console.log(`upserting data: ${JSON.stringify(dataOrString)}`);

    return new Promise((resolve, reject) => {
        Model.findOneAndUpdate(query, data, { upsert: true }, (err, result) => {
            if (err) { 
                console.info('Data upserted error', err);
                return reject(err);
            } else {
                console.info('Data upserted', result);
                return resolve(result);
            }
        });
    });
};

// DELETE
Data.delete = function(id) {
    console.log(`deleting data: ${id}`);

    return new Promise((resolve, reject) => {
        Model.deleteOne({ _id: id }, (err, result) => {
            if (err) { 
                console.info('Data deleted error', err);
                return reject(err);
            } else {
                console.info('Data deleted', id);
                //result: { n: 1, ok: 1 }
                return resolve(id);
            }            
        });
    });
};

// RESET WHOLE COLLECTION
Data.reset = function() {
    console.log('resetting all data');
    return new Promise((resolve, reject) => {
        //TODO:
        // try {
        //     Model.collection.drop();
        //     callback(null, 'data collection dropped');
        // } catch (ex) {
        //     callback(ex);
        // }
        Model.collection.deleteMany({}, (err, result) => {
            if (err) { 
                console.info('Data resetted error', err);
                return reject(err);
            } else {
                console.info('Data resetted', 'success');
                //result: { result: { n: 0, ok: 1 }, + much more }
                return resolve('success');
            }
        });
    });
};


// HELPERS
function normalizeData(dataOrString) {
    let data;
    if (typeof dataOrString === 'string') {
        data = parseString(dataOrString);
    } else if (typeof dataOrString === 'object') {
        data = dataOrString;
    }
    return data;
}

function parseString(str) {
    try {
        const array = str.split(' ');
        const parsed = {
            value: [],
            tags: [],
            users: []
        };
        for (let item of array) {
            item = item.trim();
            if (item) {
                if (item.startsWith('#') && item.length > 1) {
                    parsed.tags.push(item.substr(1));
                } else if (item.startsWith('@') && item.length > 1) {
                    parsed.users.push(item.substr(1));
                } else {
                    parsed.value.push(item);
                }
            }
        }
        parsed.value = parsed.value.join(' ');
        parsed.value = parsed.value || '';
        return parsed;
    } catch (ex) {
        console.error(`parseString error: ${str}`);
    }
}
module.exports = Data;
