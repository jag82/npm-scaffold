// TODO update to return promises (see template.data.js)
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const SALT_ROUNDS = 12;

const Model = mongoose.model('User', new mongoose.Schema({
    name: { 
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    abilities: {
        type: mongoose.Schema.Types.Mixed,
        default: {}
    }
}));

const Users = {};

// CREATE
Users.create = function(user, callback) {
    const { name, password } = user;
    console.log(`creating user: ${name}`);

    bcrypt
        .hash(password, SALT_ROUNDS)
        .then((hash) => {
            Model.create({ name, hash }, (err) => {
                console.info('New user added');
                if (typeof callback === 'function') {
                    return callback(err, name);            
                }
            });
        });
};

// READ
Users.findOne = function(name, callback) {
    console.log(`finding one user: ${name}`);
    if (!name) {
        return callback('user not found');
    }
    Model
        .findOne({ name })
        .select('name abilities')
        .exec((err, result) => {
            if (typeof callback === 'function') {
                return callback(err, result);            
            }
        });
};

// ACCOUNTS
Users.login = function(name, password, callback) {
    console.log(`logging in user: ${name}`);

    Model
        .findOne({ name })
        .exec((err, result) => {
            if (err || !result) {
                console.log('user not found');
                if (typeof callback === 'function') {
                    return callback(err || 'user not found', result);
                }
            } else {
                console.log('user found');
                const hash = result.hash;
                result = null;
                bcrypt
                    .compare(password, hash)
                    .then((isCorrect) => {
                        if (isCorrect) {
                            result = { name };
                        } else {
                            err = 'incorrect password';
                        }
                        if (typeof callback === 'function') {
                            return callback(err, result);
                        }
                    });
            }
        });
};

Users.logout = function(name, password, callback) {
    console.log(`logging out user: ${name}`);

    //TODO: actually logout (session?)
    Model
        .find({ name })
        .exec((err, result) => {
            console.log(JSON.stringify(result, null, 4));
            if (typeof callback === 'function') {
                return callback(err, result);
            }
        });
};





module.exports = Users;
