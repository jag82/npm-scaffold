console.log(`
    ----------------------
    You are using MongoDB.
    Make sure you install and run it before running your app!
    
    # Install
        https://www.mongodb.com/docs/v4.2/installation/

        wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -

        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

        sudo apt-get update

        sudo apt-get install -y mongodb-org

    # Run
        (local)
        mongod --port 27017 --dbpath ./database

        (production)
        sudo systemctl start mongod

    # Confirm
        sudo systemctl status mongod

    # Restart on Crash
        sudo systemctl enable mongod

    # Stop
        sudo systemctl stop mongod
    ----------------------
`);
const mongoose = require('mongoose');

mongoose.connection.on('connected', function() {  
    console.log('Mongoose connection open ');
    db.handlers.onConnected.forEach((fn) => {
        fn();
    });
}); 

mongoose.connection.on('error', function(err) {  
    console.log(`Mongoose connection error: ${err}`);

    console.log(`
        Perhaps the database isn't running. To start mongodb, open a seperate terminal and try:
            mongod
        or
            mongod --port 27017 --dbpath ./database

        Then restart this web app:
            node web.js
    `);

    db.handlers.onError.forEach((fn) => {
        fn(err);
    });
}); 

mongoose.connection.on('disconnected', function() {  
    console.log('Mongoose connection disconnected'); 
    db.handlers.onDisconnected.forEach((fn) => {
        fn();
    });
});

process.on('SIGINT', function() {  
    mongoose.connection.close(function() { 
        console.log('Mongoose connection disconnected through app termination'); 
        process.exit(0); 
    }); 
}); 

const db = {};

db.handlers = {
    onConnected: [],
    onError: [],
    onDisconnected: []
};

db.connect = function(uri, callback) {
    db.handlers.onConnected.push(callback);
    // strictQuery true = only save fields defined in models to db (no surprises)
    // strictQuery false = save any arbitrary field (more flexible)
    mongoose.set('strictQuery', false);
    mongoose.connect(uri);
};

db.close = function() {
    mongoose.connection.close(function() {
        console.log('Mongoose connection disconnected through db.close()'); 

    });
};

module.exports = db;
