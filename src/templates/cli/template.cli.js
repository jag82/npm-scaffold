const pkg = require('../package.json');
const yargs = require('yargs');

yargs
    .scriptName('${PROJECT_NAME}')
    .usage('$0 <command> [args]', pkg.description)

    .showHelpOnFail() // failure? let's show the auto generated help
    .demandCommand(1, '') // no command given? we'll call this a failure (and trigger the help...)
    .alias('help', 'h') // ${PROJECT_NAME} help, --help, -h all return help
    .alias('version', 'v') // ${PROJECT_NAME} version, --version, -v all return help
    .wrap(yargs.terminalWidth()) //maximize width

    // ${PROJECT_NAME} speak hello
    .command('speak [text]', 'output text', (yargs) => {
        yargs.positional('text', {
            type: 'string',
            default: 'you did not give me any input',
            describe: 'additional yelling text'
        });
    }, (args) => {
        console.log(`I AM SO LOUD! ${args.text.toUpperCase()}!!` );    
    })

    // ${PROJECT_NAME} listen
    .command('listen', 'get user inputs', (yargs) => {

    }, require('./cli/commands/listen.js'))

    // if you don't want to use commands, replace usage() and remove demandCommand()
    // ${PROJECT_NAME}
    /*
    // <parameterName> = required parameter
    // [parameterName] = optional parameter
    .usage('$0 <project> [description]', 'Creates a new project with useful libraries and patterns.', (yargs) => {
        yargs.positional('project', {
          describe: 'The name of your project (a folder will be created with this name).',
          type: 'string'
        });
        yargs.positional('description', {
          describe: 'A short description of your project.',
          type: 'string',
          default: 'Undescribed project.'
        });
    })
    */

const args = yargs.parse();
