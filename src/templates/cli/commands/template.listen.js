const { prompt } = require('inquirer');

const questions = [
    {
        type: 'input',
        name: 'firstname',
        message: 'Enter firstname ...'
    },
    {
        type: 'input',
        name: 'lastname',
        message: 'Enter lastname ...'
    },
    {
        type: 'input',
        name: 'phone',
        message: 'Enter phone number ...'
    },
    {
        type: 'input',
        name: 'email',
        message: 'Enter email address ...'
    }
];

const listen = function(args) {
    prompt(questions).then((answers) => {
        console.log(answers);
    });
};

module.exports = listen;
