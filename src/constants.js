const FILES_PATH = 'app/files';
const DATABASE_PATH = 'database';

module.exports = {
    FILES_PATH,
    DATABASE_PATH
};
