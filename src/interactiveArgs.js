const { prompt } = require('inquirer');
const { log } = require ('./utils.js');

async function interactiveArgs(args) {
    log(`interactiveArgs`, 'title');

    // Setup questions
    const questions = [];
    
    questions.push({
        name: 'project',
        message: 'What is the name of this project?',
        type: 'input',
        default: args.project
    });

    const interfacesDefaults = [];
    if (args.cli) {
        interfacesDefaults.push('cli');
    }
    if (args.web) {
        interfacesDefaults.push('web');
    }
    questions.push({
        name: 'interfaces',
        message: 'What kind of interface will your app have?',
        type: 'checkbox',
        choices: [
            { name: 'Web App', value: 'web' },
            { name: 'Commandline Interface', value: 'cli' }
        ],
        default: interfacesDefaults
    });

    addQuestion('db', 'store information in a database');
    addQuestion('files', 'store files');
    addQuestion('settings', 'have a settings.json for sensitive credentials', true);

    addQuestion('readme', 'have a README', true);
    addQuestion('tests', 'contain tests', true);
    addQuestion('repo', 'use a version control repository', true);
    addQuestion('linter', 'use a linter', true);

    // addQuestion('config', 'reference a base config');
    // addQuestion('save', 'use a save');

    function addQuestion(id, text, defaultValue = false) {
        questions.push({
            name: id,
            message: `Will your app ${text}?`,
            type: 'confirm',
            default: typeof args[id] === 'boolean' ? args[id] : defaultValue
        })
    }


    // Get answers
    const answers = await prompt(questions);

    const interactiveArgs = {
        project: answers.project,
        cli: answers.interfaces.includes('cli'),
        web: answers.interfaces.includes('web'),
        db: answers.db,
        files: answers.files,
        settings: answers.settings,

        readme: answers.readme,
        tests: answers.tests,
        repo: answers.repo,
        linter: answers.linter,

        config: false,
        save: false,
        interactive: true
    }

    // what would the equivalent commandline be?
    const options = [];
    const excludedOptions = ['project', 'interactive'];
    for (let key of Object.keys(interactiveArgs)) {
        if (excludedOptions.includes(key)) {
            continue;
        }
        if (interactiveArgs[key]) {
            options.push(`--${key}`);
        } else {
            options.push(`--no-${key}`);
        }
    }
    log('');
    log('to repeat this from the commandline:', 'comment')
    log(`npm-scaffold ${interactiveArgs.project} ${options.join(' ')}`, 'code');
    log('');

    log(interactiveArgs, 'json');
    log('\n');

    return interactiveArgs;
}

module.exports = interactiveArgs;
