const yargs = require('yargs');
const { log } = require ('./utils.js');

function parseArgs() {
    log(`parseArgs`, 'title');

    const args = yargs
        .scriptName('npm-scaffold')

        .usage('$0 <project> [description]', 'Creates a new project with useful libraries and patterns.', (yargs) => {
            yargs.positional('project', {
              describe: 'The name of your project (a folder will be created with this name).',
              type: 'string'
            });
            yargs.positional('description', {
              describe: 'A short description of your project.',
              type: 'string',
              default: 'Undescribed project.'
            });
        })

        .option('cli', {
            group: 'App:',
            alias: 'c',
            describe: 'Your app will have a command line interface (CLI).',
            type: 'boolean'
        })
        .option('web', {
            group: 'App:',
            alias: 'w',
            describe: 'Your app will have a web interface.',
            type: 'boolean'
        })
        .option('db', {
            group: 'App:',
            alias: 'd',
            describe: 'Your app will use a database.',
            type: 'boolean'
        })
        .option('files', {
            group: 'App:',
            alias: 'f',
            describe: 'Your app will have file storage.',
            type: 'boolean'
        })
        .option('settings', {
            group: 'App:',
            describe: 'Your app will have a settings file (e.g. for credentials).',
            type: 'boolean'
        })

        .option('readme', {
            group: 'Code:',
            describe: 'Your code will have a README so that others (or yourself in the future) will know what the hell it\'s for!',
            type: 'boolean'
        })
        .option('tests', {
            group: 'Code:',
            describe: 'Your code will have automatic tests.',
            type: 'boolean'
        })
        .option('repo', {
            group: 'Code:',
            describe: 'Your code will use a repository for version control (e.g. git).',
            type: 'boolean'
        })
        .option('linter', {
            group: 'Code:',
            describe: 'Your code will have styling/syntax rules.',
            type: 'boolean'
        })
        //npm .user .author .publish?

        .option('interactive', {
            alias: 'i',
            describe: 'Decide how you want to generate your app via interactive prompt.',
            type: 'boolean'
        })
        .option('save', {
            alias: 's',
            describe: 'Save config (including option flags) to a locally saved config.'
        })
        .option('config', {
            describe: 'Provide a config with custom settings for scaffold generation.'
            //TODO: check if path is valid!
        })

        .showHelpOnFail()
        .demandCommand(1, '')
        .alias('help', 'h')
        .alias('version', 'v')
        .wrap(yargs.terminalWidth())
        .argv;

    log(args, 'json');
    log('\n');

    // Shall we run in interactive mode to prompt the user for further args?
    if (args.interactive) {
        const interactiveArgs = require('./interactiveArgs.js');
        return interactiveArgs(args);
    } else {
        return args;        
    }
}

module.exports = parseArgs;
