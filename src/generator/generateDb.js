const { writeOutput, readTemplate, copy } = require('./utils.js');
const { DATABASE_PATH } = require('../constants.js');

async function generateDb(options) {
    const packageJson = options.packageJson;
    const repoIgnore = options.repoIgnore;

    packageJson.scripts['db-start'] = `mkdir -p ${DATABASE_PATH} && mongod --port 27017 --dbpath ./database`;

    packageJson.dependencies['mongoose'] = '6.8.1';

    // entry point (db connection)
    writeOutput(DATABASE_PATH, null, options);
    repoIgnore.push(DATABASE_PATH);
    const dbTemplate = await readTemplate('core/template.db.js', options);
    writeOutput('app/core/db.js', dbTemplate, options);

    // models
    const dataTemplate = await readTemplate('core/models/template.data.js', options);
    writeOutput('app/core/models/data.js', dataTemplate, options);

    const usersTemplate = await readTemplate('core/models/template.users.js', options)
    writeOutput('app/core/models/users.js', usersTemplate, options);
}

module.exports = generateDb;
