const { writeOutput, readTemplate } = require('./utils.js');

async function generateTests(options) {
    const packageJson = options.packageJson;

    packageJson.scripts['test'] = 'tape test/**/*.tests.js | tap-notify | tap-spec || true';
    packageJson.scripts['test-watch'] = 'chokidar \"**/*.js\" -i \"node_modules\" -c \"npm run test\"';

    // TODO update chokidar-cli, tape?
    packageJson.devDependencies['chokidar-cli'] = '1.2.2';
    packageJson.devDependencies['tap-notify'] = '1.0.0';
    packageJson.devDependencies['tap-spec'] = '5.0.0';
    packageJson.devDependencies['tape'] = '4.9.1';
    packageJson.devDependencies['tape-async'] = '2.3.0';

    const testsTemplate = await readTemplate('test/template.all.tests.js', options);
    writeOutput('test/all.tests.js', testsTemplate, options);
}

module.exports = generateTests;
