const fs = require('fs');
const path = require('path');
const LineByLineReader = require('line-by-line');
const { FILES_PATH } = require('../constants.js');

const TEMPLATE_DIR = path.join(__dirname, '../templates/');
const BINARY_FILES = ['.gif', '.jpg', '.jpeg', '.png', '.svg', '.eot', '.ttf', '.woff', '.woff2'];
const LOG = {
    VERBOSE: -1,
    INFO: 0,
    WARNING: 1,
    ERROR: 2
};

function copy(src, dst, options) {
    const templatePath = path.join(TEMPLATE_DIR, src);
    const outputPath = path.join(options.outputDir, dst);

    ensureDirectoryExistence(outputPath, true);

    const templateStat = fs.lstatSync(templatePath);
    const outputStat = fs.lstatSync(outputPath);

    if (templateStat.isDirectory() && outputStat.isDirectory()) {
        fs.readdirSync(templatePath).forEach(async (fileName) => {
            const template = await readTemplate(path.join(src, fileName), options);
            writeOutput(path.join(dst, fileName), template, options);
        });      
    } else {
        writeOutput(dst, src, options);
    }
}



function readTemplate(filePath, options) {
    return new Promise((resolve, reject) => {
        const templatePath = path.join(TEMPLATE_DIR, filePath);

        const ext = path.extname(filePath);
        if (BINARY_FILES.includes(ext)) {
            resolve(fs.readFileSync(templatePath));
        } else {
            let contents = '';
            let directive = false;
            let directiveBlock = false;
            let directiveElse = false;
            let writeLine = false;

            log(`readTemplate: ${templatePath}`, LOG.INFO, options);
            const lr = new LineByLineReader(templatePath);

            // use directives to conditionally exclude parts of templates (e.g. db api imports can be excluded from web server code if db is not enabled)
            lr.on('line', function (line) {
                log(`line: ${line}`, LOG.VERBOSE, options);
                // lr.pause();
                writeLine = false;

                // DEFINE DIRECTIVES
                if (/^\/\/@[A-Za-z0-9]+\s*$/.test(line)) {
                    //@db
                    // = skip next line if db option not chosen
                    directive = line.substr(3).trim(); //db
                    directiveBlock = false;
                    log(`found directive ${directive}`, LOG.INFO, options);
                } else if (/^\/\/@@(?!else)[A-Za-z0-9]+\s*$/.test(line)) {
                    //@@db  (but not //@@else)
                    // = skip all lines until end directive (//@@) if db option not chosen
                    directive = line.substr(4); //db
                    directiveBlock = true;
                    directiveElse = false;
                    log(`found directiveBlock: ${directive}`, LOG.INFO, options);
                } else if (/^\/\/@@else\s*$/.test(line)) {
                    //@@else
                    // = alternative lines if db option is not selected
                    directiveElse = true;
                    log(`found directiveElse: ${directive}`, LOG.INFO, options);
                } else if (/^\/\/@@\s*$/.test(line)) {
                    //@@
                    // = end of line skipping
                    directive = false;
                    log(`end directive: ${directive}`, LOG.INFO, options);
                } else {
                // RESPOND TO DIRECTIVE (or absence of)
                    if (!directive) {
                        // no directive, write the line
                        writeLine = true;
                    } else {
                        const optionEnabled = options[directive];
                        if (!directiveBlock) {
                            // simple directive, skip if matching option is off (automatically disable after this line)
                            writeLine = optionEnabled;
                            directive = false;
                        } else {
                            if (!directiveElse) {
                                // block directive (if), skip if matching option is off
                                writeLine = optionEnabled;    
                            } else {
                                // block directive (else), skip if matching option is on
                                writeLine = !optionEnabled;
                            }
                        }
                    }
                }
                // FINALLY, WRITE LINE IF APPOPRIATE
                if (writeLine) {
                    //TODO: option to write to filestream 
                    contents += `${line}\n`;
                }
                // lr.resume();
            });
            lr.on('end', function () {
                resolve(
                    contents
                        .replace(/\${PROJECT_NAME}/g, options.project)
                        .replace(/\${PROJECT_DESCRIPTION}/g, options.description)
                        .replace(/\${FILES_PATH}/g, FILES_PATH)
                );
            });
            lr.on('error', function (err) {
                 reject(err);
            });
        }        
    })
}

function writeOutput(filePath, contents, options) {
    const outputPath = path.join(options.outputDir, filePath);
    ensureDirectoryExistence(outputPath);
    if (contents) {
        fs.writeFileSync(outputPath, contents);
    }
} 

function ensureDirectoryExistence(filePath, topLevelIsDir = false) {
    const dirname = topLevelIsDir ? filePath : path.dirname(filePath);
    
    if (!fs.existsSync(dirname)) {
        ensureDirectoryExistence(dirname);
        // fs.mkdirSync(dirname);
        fs.mkdirSync(dirname, { recursive: true });
    }    
}

function log(message, writeLevel = LOG.INFO, options = {}) {
    const readLevel = typeof options.logLevel !== 'undefined' ? options.logLevel : LOG.WARNING;
    if (writeLevel >= readLevel) {
        console.log('[generator.utils.log] ', message);
    }
}

module.exports = {
    copy,
    readTemplate,
    writeOutput,
    ensureDirectoryExistence,
    log,
    LOG
};
