const { writeOutput, readTemplate } = require('./utils.js');

async function generateSettings(options) {
    const repoIgnore = options.repoIgnore;
    
    repoIgnore.push('settings.json');

    const settingsTemplate = await readTemplate('template.settings.json', options);
    writeOutput('settings.json', settingsTemplate, options);
}

module.exports = generateSettings;
