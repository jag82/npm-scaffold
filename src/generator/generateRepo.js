const { writeOutput } = require('./utils.js');

function generateRepo(options) {
    const packageJson = options.packageJson;
    const projectName = options.project;
    const repoUser = options.repo.user;
    const repoSite = options.repo.site;
    const repoIgnore = options.repoIgnore;

    if (repoSite === 'gitlab') {
        packageJson.repository = {
            type: 'git',
            url: `git+https://gitlab.com/${repoUser}/${projectName}.git`
        };
        packageJson.homepage = `https://gitlab.com/${repoUser}/${projectName}#readme`;
        packageJson.bugs = {
            url: `https://gitlab.com/${repoUser}/${projectName}/issues`
        };
    } else {
        //TODO: add this back once we actually reference remote repos
        // throw `unsupported repo site: ${repoSite}`;
    }
    repoIgnore.push(''); //end with newline
    writeOutput('.gitignore', repoIgnore.join('\n'), options);
}

module.exports = generateRepo;
