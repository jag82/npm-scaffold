const { writeOutput, readTemplate, copy } = require('./utils.js');
const { 
    FILES_PATH,
    DATABASE_PATH,
} = require('../constants.js');

async function generateWeb(options) {
    const packageJson = options.packageJson;
    const projectName = options.project;
    const repoIgnore = options.repoIgnore;

    // useful scripts/dependencies for web
    let startCommands = 'npm i';  // install dependencies
    if (options.files) {
        startCommands += ` && mkdir -p ${FILES_PATH}`
    }
    startCommands += ' && node app/web.js'; // start web server

    packageJson.scripts['web-start'] = startCommands;
    
    packageJson.scripts['web-live'] = 'npm run start -- 8080';
    packageJson.scripts['web-watch'] = 'nodemon app/web.js --ignore app/sessions/';
    packageJson.scripts['web-debug'] = 'node --inspect app/web.js'; //Chrome > about:inspect > Open dedicated DevTools for Node

    // TODO update or remove nodemon?
    packageJson.dependencies['express'] = '4.18.2';
    packageJson.dependencies['node-cron'] = '3.0.2';
    packageJson.dependencies['express-session'] = '1.17.3';
    packageJson.dependencies['session-file-store'] = '1.5.0';
    packageJson.devDependencies['nodemon'] = '1.18.10';
    if (options.db) {
        packageJson.dependencies['bcrypt'] = '5.1.0';
    }
    if (options.files) {
        packageJson.dependencies['multer'] = '1.4.5-lts.1';
    }

    repoIgnore.push('app/web/sessions');

    //entry point
    const webTemplate = await readTemplate('web/template.main.js', options);
    writeOutput('app/web.js', webTemplate, options);

    // server code
    //TODO: out-of-date (is it really out of date?)
    const setupTemplate = await readTemplate('web/server/template.setup.sh', options);
    writeOutput('app/web/server/setup.sh', setupTemplate, options);

    const appTemplate = await readTemplate('web/server/template.app.js', options);
    writeOutput('app/web/server/app.js', appTemplate, options);

    const cronJobsTemplate = await readTemplate('web/server/template.cron-jobs.js', options);
    writeOutput('app/web/server/cron-jobs.js', cronJobsTemplate, options);

    const routesTemplate = await readTemplate('web/server/routes/template.routes.js', options);
    writeOutput('app/web/server/routes/routes.js', routesTemplate, options);

    const utilsTemplate = await readTemplate('web/server/api/template.utils.js', options);
    writeOutput('app/web/server/api/utils.js', utilsTemplate, options);

    if (options.db) {
        const dataTemplate = await readTemplate('web/server/api/template.data.js', options);
        writeOutput('app/web/server/api/data.js', dataTemplate, options);

        const usersTemplate = await readTemplate('web/server/api/template.users.js', options);
        writeOutput('app/web/server/api/users.js', usersTemplate, options);
    }
    if (options.files) {
        const filesTemplate = await readTemplate('web/server/api/template.files.js', options);
        writeOutput('app/web/server/api/files.js', filesTemplate, options);
    }

    // client code
    const indexHtmlTemplate = await readTemplate('web/client/template.index.html', options);
    writeOutput('app/web/client/index.html', indexHtmlTemplate, options);

    const indexJsTemplate = await readTemplate('web/client/template.index.js', options)
    writeOutput('app/web/client/index.js', indexJsTemplate, options);

    const indexCssTemplate = await readTemplate('web/client/template.index.css', options);
    writeOutput('app/web/client/index.css', indexCssTemplate, options);

    const jQueryTemplate = await readTemplate('web/client/js/jquery-3.3.1.min.js', options);
    writeOutput('app/web/client/js/jquery-3.3.1.min.js', jQueryTemplate, options);

    const fontAwesomeTemplate = await readTemplate('web/client/styles/fontawesome.min.css', options);
    writeOutput('app/web/client/styles/fontawesome.min.css', fontAwesomeTemplate, options);

    copy('web/client/webfonts', 'app/web/client/webfonts', options);
    copy('web/client/favicon', 'app/web/client/favicon', options);

    const pages = ['create', 'edit', 'list', 'view'];
    for (let page of pages) {
        const htmlTemplate = await readTemplate(`web/client/pages/${page}/template.${page}.html`, options);
        writeOutput(`app/web/client/pages/${page}/${page}.html`, htmlTemplate, options);
        const jsTemplate = await readTemplate(`web/client/pages/${page}/template.${page}.js`, options);
        writeOutput(`app/web/client/pages/${page}/${page}.js`, jsTemplate, options);
        const cssTemplate = await readTemplate(`web/client/pages/${page}/template.${page}.css`, options);
        writeOutput(`app/web/client/pages/${page}/${page}.css`, cssTemplate, options);
    }

    if (options.db) {
        const loginHtmlTemplate = await readTemplate('web/client/pages/login/template.login.html', options);
        writeOutput('app/web/client/pages/login/login.html', loginHtmlTemplate, options);        

        const loginJsTemplate = await readTemplate('web/client/pages/login/template.login.js', options);
        writeOutput('app/web/client/pages/login/login.js', loginJsTemplate, options);        
    }
}

module.exports = generateWeb;
