const { writeOutput, readTemplate } = require('./utils.js');

async function generateReadme(options) {
    const readmeTemplate = await readTemplate('template.README.md', options);
    writeOutput('README.md', readmeTemplate, options);        
}

module.exports = generateReadme;
