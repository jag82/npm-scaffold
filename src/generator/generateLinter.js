const { writeOutput, readTemplate } = require('./utils.js');

async function generateLinter(options) {
    const linterTemplate = await readTemplate('template.eslintrc.json', options);
    writeOutput('.eslintrc.json', linterTemplate, options);
}

module.exports = generateLinter;
