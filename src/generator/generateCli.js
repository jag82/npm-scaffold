const { writeOutput, readTemplate } = require('./utils.js');

async function generateCli(options) {
    const packageJson = options.packageJson;
    const projectName = options.project;

    packageJson.bin = `app/cli/bin/${projectName}`;

    // TODO upgrade packages?
    packageJson.dependencies['yargs'] = '13.2.2';
    packageJson.dependencies['inquirer'] = '6.2.0';
    packageJson.dependencies['chalk'] = '2.4.2';
    //clui? readline?

    const binTemplate = await readTemplate('cli/bin/template.bin', options);
    writeOutput(`app/cli/bin/${projectName}`, binTemplate, options);

    const cliTemplate = await readTemplate('cli/template.cli.js', options);
    writeOutput('app/cli.js', cliTemplate, options);

    writeOutput('app/cli/commands', null, options);

    const speakTemplate = await readTemplate('cli/commands/template.speak.js', options);
    writeOutput('app/cli/commands/speak.js', speakTemplate, options);

    const writeTemplate = await readTemplate('cli/commands/template.listen.js', options);
    writeOutput('app/cli/commands/listen.js', writeTemplate, options);
}

module.exports = generateCli;
