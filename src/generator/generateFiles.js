const { writeOutput, readTemplate } = require('./utils.js');
const { FILES_PATH } = require('../constants.js');

async function generateFiles(options) {
    const packageJson = options.packageJson;
    const projectName = options.project;
    const repoIgnore = options.repoIgnore;

    // useful scripts/dependencies for web
    // ...

    // transferrable files
    repoIgnore.push(FILES_PATH);
    writeOutput(FILES_PATH, null, options);

    const penguinImage = await readTemplate('files/template.penguin.gif', options);
    writeOutput(`${FILES_PATH}/default/penguin.gif`, penguinImage, options);
}

module.exports = generateFiles;
