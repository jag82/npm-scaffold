const fs = require('fs');
const path = require('path');
const { log } = require ('./utils.js');

function generateProject(config) {
    log('generateProject', 'title');
    // CREATE PROJECT DIR
    config.outputDir = path.join(process.cwd(), config.project);
    if (fs.existsSync(config.project)) {
        log(`Directory ${config.project} already exists. Delete it and re-run.`);
        process.exit(0);
    } 

    // INIT NPM PROJECT
    const { execSync } = require('child_process');

    execSync(`mkdir -p ${config.project}`);
    process.chdir(config.project);
    execSync('npm init -y');
    process.chdir('..');    
    const packageJson = require(path.join(config.outputDir, 'package.json'));
    delete packageJson.main;
    packageJson.description = config.description;
    packageJson.author = (config.npm && config.npm.author) || 'anonymous'; //TODO: revisit when we add deeper config settings than boolean
    packageJson.scripts = {};
    packageJson.dependencies = {};
    packageJson.devDependencies = {};




    // ADD REQUESTED COMPONENTS TO PROJECT
    // keep a running list of all things that should be ignored or added to package.json
    config.packageJson = packageJson;
    config.repoIgnore = [ 
        'node_modules',
        'npm-debug.log',
        '*.orig'
    ];

    if (config.settings) {
        require('./generator/generateSettings.js')(config);
    }
    if (config.readme) {
        require('./generator/generateReadme.js')(config);
    }
    if (config.linter) {
        require('./generator/generateLinter.js')(config);
    }
    if (config.db) {
        require('./generator/generateDb.js')(config);
    }
    if (config.files) {
        require('./generator/generateFiles.js')(config);
    }
    if (config.web) {
        require('./generator/generateWeb.js')(config);
    }
    if (config.cli) {
        require('./generator/generateCli.js')(config);
    }
    if (config.tests) {
        require('./generator/generateTests.js')(config);
    }
    if (config.repo) {
        require('./generator/generateRepo.js')(config);
    }

    const { writeOutput, copy } = require('./generator/utils.js');
    // update package json
    writeOutput('package.json', JSON.stringify(config.packageJson, null, 4), config);

    // COPY STATIC FILES
    fs.copyFileSync(path.join(__dirname, 'constants.js'), path.join(config.outputDir, 'app/constants.js'));

    // PRINT USEFUL COMMANDS FOR GETTINGS STARTED
    const startCommands = [];
    if (config.db) {
        startCommands.push({
            title: 'start database (separate window)',
            command: 'npm run db-start'
        });
    }
    if (config.web) {
        startCommands.push({
            title: 'web app',
            command: 'npm run web-start'
        });
    }
    if (config.cli) {
        startCommands.push({
            title: 'command line interface (CLI)',
            command: `npm link && ${config.project}`
            //TODO: 'npm unlink'
        });
    }

    log(`[ ${config.project} ] generated. To run:`);
    log(`cd ${config.project}\n`, 'code');
    for (let startCommand of startCommands) {
        log(`# ${startCommand.title}`, 'comment');
        log(`${startCommand.command}\n`, 'code');
    }

    if (config.repo) {
        log(`Remember to init your git repo:`);
        log(`git init`, 'code');
    }
}

module.exports = generateProject;
