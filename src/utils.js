const chalk = require('chalk');

function log(message, type) {
    switch (type) {
        case 'title': {
            message = chalk.bold.underline(message);
            break;
        }
        case 'json': {
            message = chalk.dim(JSON.stringify(message, null, 4));
            break;
        }
        case 'code': {
            message = chalk.inverse(message);
            break;
        }
        case 'comment': {
            message = chalk.dim(message);
            break;
        }
        default: {
            message = chalk.cyan(message);
            break;

        }
    }
    console.log(message);
}

module.exports = {
    log
};
