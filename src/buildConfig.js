const { log } = require ('./utils.js');

function buildConfig(args = {}) {
    log('buildConfig', 'title');
    // 1) start with our default config (same for all users of this package)
    const DEFAULT_CONFIG = '../default-config.json';
    const defaultConfig = require(DEFAULT_CONFIG);
    
    // 2) override with a user's local config (without having to specify the path repeatedly)
    // ~/.config/configstore/npm-scaffold.json    
    const Configstore = require('configstore');
    const configstore = new Configstore(args.$0);
    let localConfig = configstore.get();

    // 2b) a user can pass in a path to a specific local config
    // npm-scaffold <project> --config ~/some/path/config.json
    if (args.config) {
        localConfig = require(args.config);
    } else if (args.config === false) {
        log('- not using a base config file (--no-config or --interactive)');
        localConfig = {};
    }

    // 3) finally, any commandline args will override config files
    const deepExtend = require('deep-extend');
    const config = deepExtend({}, defaultConfig, localConfig, args);

    log(`- see default config options @ ${DEFAULT_CONFIG}`);
    log(`- set local config overrides @ ${configstore.path}`);
    log(config, 'json');
    log('\n');

    return config;
}

module.exports = buildConfig;
