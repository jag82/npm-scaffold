# npm-scaffold

Quickly create scaffolding for npm modules in the style I prefer (including tests!).

## Installation
```sh
npm install
npm link

# to uninstall
npm unlink 

# is this still up to date?
# npm i -g @jag82/npm-scaffold
```

## Usage
```sh
npm-scaffold myModuleName
```

For interactive promts:
```
npm-scaffold myApp -i
```


For web apps (with mongo db):
```
npm-scaffold myWebApp --web --db
```

For command line apps:
```
npm-scaffold myCliApp --cli
```

## Options

From `npm-scaffold help`:

cli
web
db
settings

readme
tests
repo
linter

interactivve
save
config


## Development

Get the code.
```
git clone git@gitlab.com:jag82/npm-scaffold.git
cd npm-scaffold
npm link
npm install
```

Changes you make will affect your global npm-scaffold. Once you're satisfied, push those changes.

Finally, if a new version is warranted:
```
npm publish //TODO
```

Argument precedence is defined @ `buildConfig.js`.  You can run interactively select commandline args via `npm-scaffold yourProject -i`.  

Code is then generated @ `generateProject.js` by calling logic @ `src/generator/*.js` which references templates @ `src/templates/template.*.*`.  

To find newer versions of packages you can either:  
- run `npm view <package>`
- create a project `npm-scaffold yourProject` and upgrade your packages via `npm update <package>@latest` (`npm outdated` helps you find out of date packages)

Then manually update the version @ `src/generator/*`.  

## TODO:
- show links to data list,create,etc. pages
- better discovery of file uploads/downloads/image tags
- test the above by recreating "is it goth" website

- file-based-db option (lowdb?)

- more unit tests for individual generate functions
- docker?
- handlebars or other html template option?

- save interactive config choices (optional)

- checkAbility should be included (use line by line to block out unused parts => directives @@db)

- db drop collection replaced with remove?

- cli should access core/models/ methods (e.g. for users), commands can still contain prompt code and whatnot (to prevent crowding of cli.js)

- --files with --cli? (what's the use case?)
- auto repo creation or instructions of what to do next (e.g. go to gitlab.com, type the following cmds)
- add notes for external deps (e.g. setting up eslint_d)
- auto npm publish?


- middleware (cors, compression, helmet): https://blog.jscrambler.com/setting-up-5-useful-middlewares-for-an-express-api/
