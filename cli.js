async function run(){
    // What commandline args are given?
    const parseArgs = require('./src/parseArgs.js');
    const args = await parseArgs();

    // What config files are available (to apply default values for any undeclared args)?
    const buildConfig = require('./src/buildConfig.js');
    const config = buildConfig(args);

    // Generate a project scaffold!
    const generateProject = require('./src/generateProject.js');
    generateProject(config);

    // 6?) post things: run tests, create/push to remote repo
    // log('post processing:');
    // TODO:
};

run();

